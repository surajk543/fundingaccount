import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
Vue.config.productionTip = false;

import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
Vue.use(Buefy);
Vue.use(VueRouter);

import Screen1 from './components/Screen1/Landingpage.vue';
import Content from './components/Screen1/Content.vue';
import Transaction from './components/Screen1/TransactionContent.vue';
import AddFundsContent from './components/Screen1/AddFundsContent.vue';

const routes = [
  {
    path:'/',
    component:Screen1,
    name:'Screen1'
  },
  {
    path:'/content',
    component:Content,
    name:'ContentPage'
  },
  {
    path:'/transaction',
    component:Transaction,
    name:'TransactionPage'
  },
  {
    path:'/AddFundsContent',
    component:AddFundsContent,
    name:'AddFundsContent'
  }
];
const router = new VueRouter({
  routes,
});
new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
